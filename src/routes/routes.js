const express = require("express");
const router = express.Router();

// User Model
const User = require('../models/user');

// GET all User
router.get('/', async (req, res) => {
    const users = await User.find();
    res.json(users);
  });

// ADD a new task
router.post('/', async (req, res) => {
    const { nombre, apellido, telefono, edad } = req.body;
    const user = new User({nombre, apellido, telefono, edad});
    await user.save();
    res.json({status: 'Task Saved'});
  });

  // GET all Tasks
router.get('/:id', async (req, res) => {
    const user = await User.findById(req.params.id);
    res.json(user);
  });

  // UPDATE a new task
router.put('/:id', async (req, res) => {
    const { nombre, apellido, telefono, edad } = req.body;
    const newUser = { nombre, apellido, telefono, edad };
    await User.findByIdAndUpdate(req.params.id, newUser);
    res.json({status: 'Task Updated'});
  });
  
  router.delete('/:id', async (req, res) => {
    await User.findByIdAndRemove(req.params.id);
    res.json({status: 'Task Deleted'});
  });

module.exports = router;