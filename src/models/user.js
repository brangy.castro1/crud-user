const mongoose = require('mongoose');
const { Schema } = mongoose;

const TaskSchema = new Schema({
  nombre: { type: String, required: true },
  apellido: { type: String, required: true },
  telefono: { type: String, required: true },
  edad: { type: String, required: true }
});

module.exports = mongoose.model('User', TaskSchema);