const mongoose = require('mongoose');
const URI = 'mongodb://localhost/usuario';

mongoose.connect(URI)
  .then(db => console.log('Db conectada'))
  .catch(error => console.error(error));

module.exports = mongoose;