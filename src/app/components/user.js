import React, { Component } from "react";
import ReactDOM from "react-dom";

class User extends Component {

    constructor(){
        super();
        this.state = {
            nombre: '',
            apellido: '',
            telefono: '',
            edad: '',
            _id: '',
            users: []
        };
        this.handleChange = this.handleChange.bind(this);
        this.addUser = this.addUser.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({
          [name]: value
        });
      }

    addUser(e){
        e.preventDefault();
        if(this.state._id) {
            fetch(`/user/${this.state._id}`, {
              method: 'PUT',
              body: JSON.stringify({
                nombre: this.state.nombre,
                apellido: this.state.apellido,
                telefono: this.state.telefono,
                edad: this.state.edad
              }),
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              }
            })
              .then(res => res.json())
              .then(data => {
                window.M.toast({html: 'Usuario actualizado', classes: 'yellow'});
                this.setState({_id: '',nombre: '', apellido: '', telefono: '', edad: ''});
                this.fetchUsers();
              });
          } else {
            fetch('/user', {
              method: 'POST',
              body: JSON.stringify(this.state),
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              }
            })
              .then(res => res.json())
              .then(data => {
                console.log(data);
                window.M.toast({html: 'Usuario guardado', classes: 'blue'});
                this.setState({nombre: '', apellido: '', telefono: '', edad: ''});
                this.fetchUsers();
              })
              .catch(err => console.error(err));
          }
    }

    deleteUser(id) {
        if(confirm('¿Seguro que quieres borrarlo?')) {
          fetch(`/user/${id}`, {
            method: 'DELETE',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          })
            .then(res => res.json())
            .then(data => {
              console.log(data);
              M.toast({html: 'Usuario Eliminado', classes: 'red'});
              this.fetchUsers();
            });
        }
      }

      editUser(id) {
        fetch(`/user/${id}`)
          .then(res => res.json())
          .then(data => {
            console.log(data);
            this.setState({
              nombre: data.nombre,
              apellido: data.apellido,
              telefono: data.telefono,
              edad: data.edad,
              _id: data._id
            });
          });
      }

    componentDidMount() {
        this.fetchUsers();
      }
    
      fetchUsers() {
        fetch('/user')
          .then(res => res.json())
          .then(data => {
            this.setState({users: data});
            console.log(this.state.users);
          });
      }

    render(){
        return(
            <div className="container">
                <h1>Usuario</h1>
                <div className="row">
                <div className="col s5">
                    <form onSubmit={this.addUser}>
                        <div className="row">
                            <div className="input-field col s12">
                            <input  onChange={this.handleChange} 
                                    id="nombre" 
                                    type="text"
                                    name="nombre" 
                                    value={this.state.nombre}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                            <input onChange={this.handleChange} 
                                    id="apellido" 
                                    type="text"  
                                    name="apellido"
                                    value={this.state.apellido} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                            <input onChange={this.handleChange}  
                                    id="telefono" 
                                    type="text"
                                    name="telefono" 
                                    value={this.state.telefono}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                            <input onChange={this.handleChange} 
                                    id="edad" 
                                    type="text" 
                                    name="edad"
                                    value={this.state.edad} />
                            </div>
                        </div>
                        <button className="btn blue accent-4" type="submit" name="action">Submit
                            <i className="material-icons right">send</i>
                        </button>
                    </form>
                </div>
                <div className="col s7">
                    <table>
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>telefono</th>
                            <th>Edad</th>
                        </tr>
                        </thead>
                        <tbody>
                        { 
                            this.state.users.map(user => {
                            return (
                                <tr key={user._id}>
                                <td>{user.nombre}</td>
                                <td>{user.apellido}</td>
                                <td>{user.telefono}</td>
                                <td>{user.edad}</td>
                                <td>
                                    <button onClick={() => this.deleteUser(user._id)} className="btn red accent-4">
                                    <i className="material-icons">delete</i> 
                                    </button>
                                    <button onClick={() => this.editUser(user._id)} className="btn light-blue darken-4" style={{margin: '4px'}}>
                                    <i className="material-icons">edit</i>
                                    </button>
                                </td>
                                </tr>
                            )
                            })
                        }
                        </tbody>
                    </table>
                </div>
                </div>
            </div>   
        );
    }
}

export default User