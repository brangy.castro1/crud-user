import React from "react";
import ReactDOM from "react-dom";

import App from './components/user';

ReactDOM.render(
    <App/>,
    document.getElementById("app")
  );